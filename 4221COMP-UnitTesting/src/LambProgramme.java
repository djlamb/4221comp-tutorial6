import java.io.FileNotFoundException;
import java.util.List;

import lamb.io.LambReader;
import lamb.io.LambTextFileReader;
import lamb.model.Lamb;
import lamb.view.LambViewer;

/**
 * LambProgramme - pulls all the other classes together.
 * 
 * @author david
 *
 */
public class LambProgramme {
	LambViewer view = new LambViewer();
	LambReader read = new LambTextFileReader();

	public static void main(String[] args) throws FileNotFoundException {
		LambProgramme prog = new LambProgramme();
		prog.countFlock();
	}

	/**
	 * Counts and displays the flock associated with the current LambReader
	 * 
	 * @throws FileNotFoundException
	 */
	public void countFlock() throws FileNotFoundException {
		List<Lamb> lambs = read.readLambs("lambs.txt");
		view.show(lambs);
		System.out.println("We have " + lambs.size() + " lambs");
	}

}
