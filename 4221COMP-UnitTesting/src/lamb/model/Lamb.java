package lamb.model;

/**
 * A simple data object representing a Lamb
 * 
 * @author david
 */
public class Lamb {
	/**
	 * A Gender Identity for Sheep and Lambs. RAM (male) or EWE (female)
	 */
	public enum Gender {
		RAM, EWE
	};

	/**
	 * The Gender identity of this Lamb. As per values found within
	 * {@link Gender}; can also be null
	 */
	public Gender gender;
	/**
	 * Indicates if the Lamb is Woolly - true if so, false if shorn
	 */
	public boolean woolly;
	/**
	 * Indicates the name of the Lamb.
	 */
	public String name;

	/**
	 * Constructs a new Lamb object with the specified parameters
	 * 
	 * @param name
	 *            the Lamb's name
	 * @param wool
	 *            true if the lamb is woolly; false if they've been shorn
	 * @param gender
	 *            the applicable Gender identity for the Lamb
	 */
	public Lamb(String name, boolean wool, Gender gender) {
		this.name = name;
		this.woolly = wool;
		this.gender = gender;
	}
}
