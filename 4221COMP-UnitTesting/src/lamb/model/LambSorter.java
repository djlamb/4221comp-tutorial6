package lamb.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A class that is responsible for sorting Lambs in a flock
 * 
 * @author david
 */
public class LambSorter {

	private List<Lamb> flock;

	/**
	 * Gets the flock; if any sorts have been applied, they'll be reflected in
	 * this flock.
	 * 
	 * @return a List of Lamb objects
	 */
	public List<Lamb> getFlock() {
		return flock;
	}

	/**
	 * Constructs a new LambSorter around a given flock of Lambs.
	 * 
	 * @param lambs
	 *            the flock of Lambs - the flock is copied on construction.
	 *            Changes to individual lambs will be reflected in this set, but
	 *            changes to the flock as a whole (e.g. adding a Lamb / removing
	 *            a Lamb) will not
	 */
	public LambSorter(List<Lamb> lambs) {
		this.flock = new ArrayList<>(lambs);
	}

	/**
	 * Sort the flock by its Woolly-ness (Woolly first, Shorn last).
	 * The changes will be found in the flock, via {@link #getFlock()}
	 */
	public void sortWoolly() {
		// This is a sort - we saw these in intro to programming
		// it uses a "Comparator" - a Strategy Design Pattern that allows one 
		// algorithm to work with all sorts of sort orders
		// we'll learn more about these in Level 5 - with Data Structures and Algorithms
		Collections.sort(flock, new Comparator<Lamb>() {
			@Override
			public int compare(Lamb o1, Lamb o2) {
				if (o1.woolly && !o2.woolly)
					return -1;
				else if (o1.woolly && o2.woolly)
					return 0;
				else
					return 1;
			}
		});
	}
}
