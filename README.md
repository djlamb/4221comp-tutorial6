# 4221COMP-tutorial6
This is a simple repository that contains some test code for the 4221COMP Unit Testing tutorial. 

At this stage in development, you might find it difficult to identify areas of your own project to integrate unit tests. This file reader code is designed to allow you to try both Unit Testing and TDD.

Students are first asked to create a unit test for Lamb.io.LambTextFileReader
- then to extend this in a suite of tests

Students are then asked to refactor this code to enable TDD, including:
- extraction of an interface to describe reading from file/stream to a collection of Lamb objects
- refactoring of extant code to implement this interface
- creation of tests to test an implementer of this interface
- creation of an alternative implementation (e.g. XML deserialisation) to pass the test(s) created